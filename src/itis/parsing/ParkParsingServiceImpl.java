package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ParkParsingServiceImpl implements ParkParsingService {

    private Object Date;

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        List<String> listField = new ArrayList<>();
        List<String> value = new ArrayList<>();

        File file = new File(Paths.get(parkDatafilePath).toUri());
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (!line.equals("***")) {
                    String[] split = line.split(":");
                    listField.add(split[0]);
                    value.add(split[1]);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Class<Park> parkClass = Park.class;
        try {
            Constructor<Park> construct = parkClass.getDeclaredConstructor();
            construct.setAccessible(true);
            Park park = construct.newInstance();
            Class<? extends Park> parkClassOb = park.getClass();

            List<Field> collect = Arrays.stream(parkClassOb.getDeclaredFields())
                    .filter(ob -> ob.getAnnotations() != null)
                    .collect(Collectors.toList());

            ArrayList<ParkParsingException.ParkValidationError> parkValidationErrors = new ArrayList<>();

            for (int i = 0; i < collect.size(); i++) {
                Annotation[] annotations = collect.get(i).getAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation instanceof FieldName) {
                        for (int j = 0; j < listField.size(); j++) {
                            if (listField.get(j).equals(collect.get(i).getAnnotation(FieldName.class).value())) {
                                collect.get(i).setAccessible(true);
                                collect.get(i).set(park, value.get(j));
                            }
                        }
                    }


                    String valueStr = null;
                    for (int j = 0; j < listField.size(); j++) {
                        if (listField.get(j).equals(collect.get(i).toString())) {
                            valueStr = value.get(j);
                        }
                    }


                    if (annotation instanceof NotBlank) {
                        if (valueStr == null | valueStr.equals("")) {
                            parkValidationErrors.add(new ParkParsingException.ParkValidationError(collect.get(i).toString(), "ошибка"));
                            throw new ParkParsingException(" null или нет зачения",
                                    parkValidationErrors);
                        }else{
                            collect.get(i).setAccessible(true);
                            if(!collect.get(i).getType().equals(Date)){
                                collect.get(i).set(park, valueStr);
                            }else{
                                // дата
                            }
                        }
                    }

                    if (annotation instanceof MaxLength) {

                    }

                }


            }


        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }


        //write your code here
        return null;
    }
}
